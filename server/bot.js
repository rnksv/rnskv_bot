const TOKEN = '09273f9c9954187ae1da28a8ee6500e6b32e91e815f5c89ce57a09da64bc38d68f76621cdfd0c3e276782';

const VK = require('vk-node-sdk');
const Group = new VK.Group(TOKEN);

const mongoose = require('mongoose');
const Schema = mongoose.Schema
mongoose.connect('mongodb://rnskv:Fortest1@ds115193.mlab.com:15193/rnskv_bot', {useNewUrlParser: true});

const PlayerModel = mongoose.model('Player', new Schema({
    id: {
        type: Number,
        unique: true,
    },
    login: String,
    balance: {
        type: Number,
        default: 0,
    },
    getBonusAt: {
        type: Date,
        default: new Date()
    }
}));


class PlayerController {
    getById(id) {
        return new Promise((resolve, reject) => {
            PlayerModel.findOne({id}, (err, player) => {
                if (err) reject(err);
                resolve(player);
            })
        });
    }
    async isRegister(id) {
        let player = await this.getById(id);
        return player ? true : false;
    }
    register(playerData) {
        return new Promise((resolve, reject) => {
            let player = new PlayerModel({
                id: playerData.id,
                login: `Player${playerData.id}`
            });
            player
                .save()
                .then(data => resolve('All done'))
                .catch(err => {console.log(err.errmsg);});
        })

    }
    changeBalanceById(id, moneyDiff) {
        console.log('Изменяю баланс', id, moneyDiff);
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $inc: { balance: moneyDiff } },
            function(err) {
                if (err) reject(err);
                resolve('All done');
            });
        })
    }
    async getBonus(id) {
        this.setBonusTimeStep(id);
        this.changeBalanceById(id, 3000);
    }
    setBonusTimeStep(id) {
        return new Promise((resolve, reject) => {
            PlayerModel.updateOne({ id }, { $set: { getBonusAt: new Date() } },
                function(err) {
                    if (err) reject(err);
                    resolve('All done');
                });
        })
    }
    getNewJob() {

    }
    leaveFromJob() {

    }
    changeWorkDaysCont() {

    }
    setWorkTimeStep() {

    }
}

class ActionsController {
    async giveBonus(message) {
        let userId = message.user_id;

        let user = await playerController.getById(userId);
        let {money, getBonusAt} = user;

        let reloadTime =  1000 * 60 * 2;
        let lastGetTime = getBonusAt.getTime();
        let currentGetTime = new Date().getTime();

        let diffTime = currentGetTime - lastGetTime;
        let canGetBonus = diffTime >= reloadTime;

        if (canGetBonus) {
            message.addText(`Я должен дать бонус?`).send()
            playerController.getBonus(userId);
        } else {
            let seconds = Math.round((reloadTime - diffTime) / 1000);

            let sLeft = seconds % 60;
            let mLeft = Math.floor(seconds / 60);
            let hLeft = Math.floor(mLeft / 60);
            message.addText(`Я дам бонус через ${hLeft > 9 ? hLeft : '0' + hLeft}:${mLeft > 9 ? mLeft : '0' + mLeft}:${sLeft > 9 ? sLeft : '0' + sLeft}`).send()
        }

    };
}

const playerController = new PlayerController();
const actionsController = new ActionsController();

Group.onMessage(async (message) => {
    let userId = message.user_id;
    let userIsRegister = await playerController.isRegister(userId);

    if (!userIsRegister) {
        await playerController.register({
            id: userId
        })
        message.addText(`Регистрация завершена`).send()
    }

    if (~message.body.toLowerCase().indexOf("бонус")) {
        actionsController.giveBonus(message);
        return;
    }
});
