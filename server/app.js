'use strict';

const BOTID = 171857834;

const TOKEN = '09273f9c9954187ae1da28a8ee6500e6b32e91e815f5c89ce57a09da64bc38d68f76621cdfd0c3e276782';

const VK = require('vk-node-sdk');
const Group = new VK.Group(TOKEN);
const mongoose = require('mongoose');
const Schema = mongoose.Schema
mongoose.connect('mongodb://rnskv:Fortest1@ds115193.mlab.com:15193/rnskv_bot', {useNewUrlParser: true});

const PlayerModel = mongoose.model('Player', new Schema({
    id: Number,
    login: String,
    balance: Number,
    getBonusAt: Number,
}));


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

class Player {
    constructor(data) {
        this.data = data;
    }
    changeBalance(diff) {
        this.data.balance += diff;
    }
}

class Bot {
    constructor(Group) {
        this.group = Group;
        this.players = {};
        this.config = {
            bonusReloadTime: 4 * 60 * 60 * 1000
        };
        this.addMessagesListener(this.messageHandlers.bind(this))
    }
    async messageHandlers(messageData) {
        let senderUser = messageData.sendMessage;

        //Проверяем есть ли пользователь
        console.log('isReg', await this.userIsRegister(senderUser));
        if (!await this.userIsRegister(senderUser)) {
            console.log('Не зареган')
            this.addPlayer({
                id: senderUser.peer_id
            });
            this.sendMessage(senderUser.peer_id,
                `Поздравляем, теперь вы один из нас!
                Доступные на данный момент команды:
                - Профиль
                - Бонус
                - Cменить логин {Новый логин}
                ...
                `);
            return;
        }
        console.log('Зареган')

        if (~messageData.body.toLowerCase().indexOf("профиль")) {
            this.sendMessage(senderUser.peer_id, this.showPlayerProfile(senderUser.peer_id));
            return;
        }
        if (~messageData.body.toLowerCase().indexOf("бонус")) {
            this.sendMessage(senderUser.peer_id, this.getBonus(senderUser.peer_id));
            return;
        }
        if (~messageData.body.toLowerCase().indexOf("сменить логин") || ~messageData.body.toLowerCase().indexOf("поменять логин")) {
            let login = messageData.body.split(' ')[2];
            this.sendMessage(senderUser.peer_id, this.changePlayerLogin(senderUser.peer_id, login));
            return;
        }
    }


    addMessagesListener(handlers) {
        this.group.onMessage(handlers)
    }
    sendMessage(userId, text, photo) {
        this.group.sendMessage({user_id: userId, message: text})
    }

    async addPlayer(user) {
        let player = new PlayerModel({
            id: user.id,
            login: `Player${user.id}`,
            balance: 0,
            getBonusAt: 0
        })
        player = await player.save();
    }
    async userIsRegister(user) {

        return await PlayerModel.findOne({id: user.id}, function(err,obj) {
            return new Promise((resolve, reject) => {
                if (err) reject(err);
                console.log(obj)
                resolve(obj ? true : false)
            })
        });

    }
    getBonus(playerId) {
        let player = this.players[playerId];
        console.log(player.data.bonus);
        player.data.bonus = player.data.bonus !== 0 ? player.data.bonus : new Date().getTime() - this.config.bonusReloadTime;
        let timeDiff = new Date().getTime() - player.data.bonus;
        if (new Date().getTime() - player.data.bonus >= this.config.bonusReloadTime) {

            let winMoney = getRandomInt(50, 500) / 1000;

            player.data.bonus = new Date().getTime();
            player.changeBalance(winMoney);
            return `Вы получили бонус в размере ${winMoney} рубля!`;
        }
        let secondsLeft = Math.round(((Math.round(this.config.bonusReloadTime - timeDiff))) / 1000);

        let minutesLeft = Math.floor(secondsLeft / 60);
        secondsLeft = secondsLeft % 60;

        let hoursLeft = Math.floor(minutesLeft / 60);
        minutesLeft = minutesLeft % 60;

        hoursLeft = hoursLeft < 10 ? '0' + hoursLeft : hoursLeft;
        minutesLeft = minutesLeft < 10 ? '0' + minutesLeft : minutesLeft;
        secondsLeft = secondsLeft < 10 ? '0' + secondsLeft : secondsLeft;

        return `Бонус будет доступен через ${hoursLeft}:${minutesLeft}:${secondsLeft}`;
    }
    changePlayerLogin(playerId, login) {
        let player = this.players[playerId].data;
        player.login = login;
        return `Вы сменили ваш логин на ${login}`;
    }
    showPlayerProfile(playerId) {
        let player = this.players[playerId].data;
        return `
🥈 ID: ${player.id}
👦 Логин: ${player.login}
📈 Баланс: ${player.balance}
👔 Работа: ${player.work}
✍🏻 Бизнес: ${player.business}
📭 Дом: ${player.home}
🚘 Машина: ${player.car}
`
    }
}

let bot = new Bot(Group);

// Group.onMessage((message) => {
    // console.log('new message', message.toJSON())
    // if (!isPlayer(message.user_id)) {
    //     message.addText('Ого, кажется ты тут первый раз, что бы посмотреть свой профиль введи *Профиль*').send();
    //     return;
    // };
    // let user = getUser(message.userId);
    // // message.setTyping();
    // switch(message.body) {
    //     case 'Профиль':
    //         message.addText('Хуй тебе а не профиль, чтоб работать введи Работать, чтоб баланс посмотреть - Баланс').send()
    //         changeUserBalance(message.user_id, 20)
    //     break
    //     case 'Работать':
    //
    //         message.addText('Работаю хозяин, за 20 рублей').send()
    //         changeUserBalance(message.user_id, 20);
    //
    //         if (observer) {
    //             Group.sendMessage({user_id: observer, message: 'Привет!, твой друг щас работает'}, (messageId, error) => {
    //                 if (messageId) {
    //                     console.log('Сообщение отправлено!\n message_id: ', messageId)
    //                 } else {
    //                     console.log('Не удалось отправить сообщение', error)
    //                 }
    //             })
    //         }
    //
    //         break;
    //     case 'Игроки':
    //         console.log(users);
    //         let string = '';
    //         for (let userId in users) {
    //             let user = users[userId];
    //             console.log(user)
    //             console.log(user)
    //             string += user.name + ' - ' + user.balance + ' шекелей \n';
    //         }
    //         message.addText(string).send();
    //         break
    //     case 'Друг':
    //         addObserver(182906685) //Кидает заявку в друзья
    //         message.addText('Теперь ваш друг следит за вами').send()
    //             break;
    //     case 'Баланс':
    //         message.addText('Ваш баланс ' + user.balance + ' шекелей' ).send()
    //         break
    //     case 'фото':
    //         message.addPhoto('https://vk.com/images/gift/875/256_1.jpg').send()
    //         break
    //     case 'документ':
    //         message.addPhoto('http://vk.com/images/gift/875/256.mp4').send()
    //         break
    //     case 'ответ':
    //         message.addText('сообщение').addForward(message.id).send()
    //         break
//     }
// })

Group.onCommand('/help', (message) => { // Пример использование комманды
    message.addText('Это тестовый бот для проверки библиотеки vk-node-sdk.').send()
})